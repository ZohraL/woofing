USE mysql; 
DROP procedure IF EXISTS `mf_count`;
DELIMITER $$
USE `user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `mf_count`()
BEGIN
	
    DECLARE total_user INT;
    DECLARE total_u INT;
    
    SELECT COUNT(*) INTO total_user FROM user;
    SELECT COUNT(*) INTO total_u FROM user;
    
    SELECT total_u * 100 / total_user AS count_mf;
	
    
END$$
DELIMITER ;


USE `mysql`;
DROP procedure IF EXISTS `mf_ratio_hôte_false`;

DELIMITER $$
USE `user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `mf_ratio_hôte_false`()
BEGIN
	
    DECLARE total_hôte_false INT;
    DECLARE total_h_f INT;
    
    SELECT COUNT(*) INTO total_hôte_false FROM user;
    SELECT COUNT(*) INTO total_h_f FROM user WHERE gender='false';
    
    SELECT total_f * 100 / total_hôte_false AS ratio_hôte_false;
	
    
END$$

DELIMITER ;


